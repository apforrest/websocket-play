package org.forrest.websocket;

import java.util.HashSet;
import java.util.Set;

import javax.websocket.Endpoint;
import javax.websocket.server.ServerApplicationConfig;
import javax.websocket.server.ServerEndpointConfig;
import javax.websocket.server.ServerEndpointConfig.Builder;

/**
 *
 */
public class EndpointApplicationConfig implements ServerApplicationConfig {

    // Programmatic configuration
    @Override
    public Set<ServerEndpointConfig> getEndpointConfigs(Set<Class<? extends Endpoint>> set) {
        return new HashSet<ServerEndpointConfig>() {
            {
                add(Builder.create(DataTransferEndpointProgrammatic.class, "/listen/{uid}").build());
            }
        };
    }

    //Annotation based configuration of endpoints
    @Override
    public Set<Class<?>> getAnnotatedEndpointClasses(Set<Class<?>> scanned) {
        Set<Class<?>> results = new HashSet<>();
        for (Class<?> clazz : scanned) {
            if (clazz.getPackage().getName().startsWith("org.forrest.websocket")) {
                results.add(clazz);

            }
        }
        return results;
    }

}
