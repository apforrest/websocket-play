package org.forrest.websocket;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import org.forgerock.json.JsonValue;

/**
 * @since 1.0
 */
public final class JsonValueEncoder implements Encoder.Text<JsonValue> {

    public String encode(JsonValue jsonValue) throws EncodeException {
        return jsonValue.toString();
    }

    public void init(EndpointConfig endpointConfig) {
        // Do nothing.
    }

    public void destroy() {
        // Do nothing.
    }

}
