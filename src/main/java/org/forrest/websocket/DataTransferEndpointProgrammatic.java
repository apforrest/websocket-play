package org.forrest.websocket;

import java.io.IOException;
import java.util.Map;

import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.PongMessage;
import javax.websocket.Session;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @since 1.0
 */
public final class DataTransferEndpointProgrammatic extends Endpoint {

    private static final Logger logger = LogManager.getLogger(DataTransferEndpointProgrammatic.class);

    @Override
    public void onOpen(final Session session, EndpointConfig ec) {
        //one session instance per peer.

        Map<String, String> pathParameters = session.getPathParameters();

        int maxTextMessageBufferSize = session.getMaxTextMessageBufferSize();
        logger.debug("onMessage >> [uid=" + pathParameters.get("uid") + "] maxTextMessageBufferSize=" + maxTextMessageBufferSize);
        //session.setMaxTextMessageBufferSize(Integer.MAX_VALUE);
        //logger.debug("onMessage >> [uid=" + pathParameters.get("uid") + "] maxTextMessageBufferSize=" + maxTextMessageBufferSize);

        //Processing whole message with the help of a handler
        session.addMessageHandler(new MessageHandler.Whole<String>() {
            @Override
            public void onMessage(String text) {
                try {
                    logger.debug("onMessage >> [uid=" + pathParameters.get("uid") + "] " + text);
                    session.getBasicRemote().sendText(text);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClose(Session session, CloseReason closeReason) {
        logger.debug("onClose >> " + closeReason);
        super.onClose(session, closeReason);
    }

    @Override
    public void onError(Session session, Throwable thr) {
        logger.error("onError >> " + thr);
        super.onError(session, thr);
    }
}
