package org.forrest.websocket;

import java.io.IOException;
import java.util.Map;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import org.forgerock.json.JsonException;
import org.forgerock.json.JsonValue;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @since 1.0
 */
public final class JsonValueDecoder implements Decoder.Text<JsonValue> {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public JsonValue decode(String s) throws DecodeException {
        try {
            return new JsonValue(MAPPER.readValue(s, Map.class));
        } catch (IOException e) {
            throw new JsonException("Failed to parse json", e);
        }

    }

    public boolean willDecode(String s) {
        return s != null;
    }

    public void init(EndpointConfig endpointConfig) {
        // Do nothing
    }

    public void destroy() {
        // Do nothing
    }

}
