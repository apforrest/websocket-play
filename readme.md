Websocket Play
==============

Either run the app in jetty or tomcat:

`mvn jetty:run`

or

`mvn tomcat7:run`

Once running visit:

`http://localhost:8080/websocket/`